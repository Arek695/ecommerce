<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/product/{id}', [\App\Http\Controllers\ProductController::class,'show']);
Route::get('/products', [\App\Http\Controllers\ProductController::class,'getAll']);

Route::get('/category/{id}', [\App\Http\Controllers\CategoryController::class,'show']);
Route::get('/categories', [\App\Http\Controllers\CategoryController::class,'getAll']);

Route::post('/review/add', [\App\Http\Controllers\ReviewController::class,'add']);
