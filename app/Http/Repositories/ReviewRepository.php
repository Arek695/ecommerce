<?php


namespace App\Http\Repositories;


use App\Models\Review;

class ReviewRepository
{

    public function addReview($request)
    {
        return Review::insert([
            'author' => $request->author,
            'product_id' => $request->product_id,
            'description' => $request->description
        ]);
    }
}
