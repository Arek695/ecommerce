<?php


namespace App\Http\Repositories;


use App\Models\Media;
use App\Models\Product;
use App\Models\Review;

class ProductRepository
{

    public function getProductById($id)
    {
        return Product::with(['category','media','reviews'])->find($id);
    }

    public function getProducts()
    {
        return Product::with(['category','media','reviews'])->get();
    }

}
