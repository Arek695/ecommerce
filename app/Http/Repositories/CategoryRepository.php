<?php


namespace App\Http\Repositories;


use App\Models\Category;

class CategoryRepository
{
    public function getCategoryById($id)
    {
        return Category::with(['products','media'])->find($id);
    }

    public function getCategories()
    {
        return Category::with(['products','media'])->get();
    }
}
