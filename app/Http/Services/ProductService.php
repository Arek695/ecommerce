<?php


namespace App\Http\Services;


use App\Http\Repositories\CategoryRepository;
use App\Http\Repositories\ProductRepository;

class ProductService
{

    /**
     * @var CategoryRepository
     */
    private $cR;
    /**
     * @var ProductRepository
     */
    private $pR;

    public function __construct(ProductRepository $pR, CategoryRepository $cR){
        $this->cR = $cR;
        $this->pR = $pR;
    }

    public function getProduct($id)
    {
        $product = $this->pR->getProductById($id);
        return $product;
    }

    public function getProducts()
    {
        $products = $this->pR->getProducts();
        return $products;
    }
}
