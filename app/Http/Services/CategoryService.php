<?php


namespace App\Http\Services;


use App\Http\Repositories\CategoryRepository;
use App\Http\Repositories\ProductRepository;

class CategoryService
{
    /**
     * @var CategoryRepository
     */
    private $cR;
    /**
     * @var ProductRepository
     */
    private $pR;

    public function __construct(ProductRepository $pR, CategoryRepository $cR){
        $this->cR = $cR;
        $this->pR = $pR;
    }

    public function getCategory($id)
    {
        $category = $this->cR->getCategoryById($id);
        return $category;
    }

    public function getCategories()
    {
        $categories = $this->cR->getCategories();
        return $categories;
    }
}
