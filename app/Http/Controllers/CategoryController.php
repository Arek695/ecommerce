<?php

namespace App\Http\Controllers;

use App\Http\Services\CategoryService;
use App\Http\Services\ProductService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @var ProductService
     */
    private $pS;
    /**
     * @var CategoryService
     */
    private $cS;

    public  function __construct(ProductService $pS, CategoryService $cS)
    {
        $this->pS = $pS;
        $this->cS = $cS;
    }

    public function getAll()
    {
        $categories = $this->cS->getCategories();
        return response()->json(['data' => $categories, 'message' => 'success'],200);
    }

    public function show($id)
    {
        $category = $this->cS->getCategory($id);
        return response()->json(['data' => $category, 'message' => 'success'],200);
    }
}
