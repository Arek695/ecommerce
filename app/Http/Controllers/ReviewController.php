<?php

namespace App\Http\Controllers;

use App\Http\Repositories\ReviewRepository;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    /**
     * @var ReviewRepository
     */
    private $rR;

    public function __construct(ReviewRepository $rR){
        $this->rR = $rR;
    }

    public function add(Request $request){
        $this->rR->addReview($request);
        return response()->json(['data' => 'Opinia zostala dodana', 'status' => 'success'],200);
    }
}
