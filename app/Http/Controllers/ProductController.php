<?php

namespace App\Http\Controllers;

use App\Http\Services\CategoryService;
use App\Http\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    /**
     * @var ProductService
     */
    private $pS;
    /**
     * @var CategoryService
     */
    private $cS;

    public  function __construct(ProductService $pS, CategoryService $cS)
    {
        $this->pS = $pS;
        $this->cS = $cS;
    }

    public function getAll()
    {
        $products = $this->pS->getProducts();
        return response()->json($products,200);
    }

    public function show($id)
    {
        $product = $this->pS->getProduct($id);
        return response()->json($product,200);
    }

    public function create(Request $request)
    {

    }

    public function update(Request $request)
    {

    }

    public function delete($id)
    {

    }
}
