<?php


namespace App\Http\Helpers;


class EmailHelper
{
    /**
     * Sprawdzenie poprawności adresu e-mail ze sprawdzeniem autentyczności domeny
     *
     * @param string $mailToTest
     * @param bool $checkDNS
     * @return bool
     */
    static public function validateEmail($mailToTest, $checkDNS = TRUE) {
        $m = strtolower($mailToTest);
        $result = filter_var($m, FILTER_VALIDATE_EMAIL) && preg_match('/@.+\./', $m);
        if ($result && $checkDNS) {
            list($ename, $edomain) = explode('@', $m);
            $result = self::checkDNS($edomain);
        }
        $ename = null;
        return $result ? 1 : 0;
    }

    /**
     * odpytanie serwera DNS o domenę
     *
     * @param string $domain
     * @return bool
     */
    static public function checkDNS(string $domain) {
        $d = strtolower($domain);
        if (strpos($d, '@') !== false) {
            $d = str_replace('@', '', strstr($d, '@'));
        }
        $test = checkdnsrr($d, 'A') || checkdnsrr($d, 'MX') || checkdnsrr($d, 'CNAME') || checkdnsrr($d, 'AAAA');
        return $test ? 1 : 0;
    }
}
