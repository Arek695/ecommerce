<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = "media";

    use HasFactory;

    public function products()
    {
        return $this->belongsTo('App\Models\Product');
    }
    public function categories()
    {
        return $this->belongsTo('App\Models\Category');
    }

}
