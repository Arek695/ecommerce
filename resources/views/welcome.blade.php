<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ecommerce Panel</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>
<body>
<div class="container-fluid">
    <div class="container">
        <h2>Lista urlów</h2>
        <div class="row m-3">
            <h5>Produkty</h5>
            <div class="col-lg-3 border p-3">
                <p>url: /products</p>
                <p>Metoda: GET</p>
                <p>Żąda: nic</p>
                <p>Zwraca: array z produktami</p>
            </div>
            <div class="col-lg-3 border p-3">
                <p>url: /products/top</p>
                <p>Metoda: GET</p>
                <p>Żąda: nic</p>
                <p>Zwraca: array z trzema najlepiej sprzedającymi się produktami</p>
            </div>
            <div class="col-lg-3 border p-3">
                <p>url: /product/{id}</p>
                <p>Metoda: GET</p>
                <p>Żąda: id - ID produktu w bazie danych</p>
                <p>Zwraca: array z produktem</p>
            </div>
        </div>
        <div class="row m-3">
            <h5>Kategorie</h5>
            <div class="col-lg-3 border p-3">
                <p>url: /categories</p>
                <p>Metoda: GET</p>
                <p>Żąda: nic</p>
                <p>Zwraca: array z kategoriami</p>
            </div>
            <div class="col-lg-3 border p-3">
                <p>url: /category/{id}</p>
                <p>Metoda: GET</p>
                <p>Żąda: ID kategorii z bazy danych</p>
                <p>Zwraca: array z produktami w danej kategorii</p>
            </div>
        </div>
        <div class="row m-3">
            <h5>Opinie</h5>
            <div class="col-lg-3 border p-3">
                <p>url: /review/add</p>
                <p>Metoda: POST</p>
                <p>Żąda: "product_id","author","description"</p>
                <p>Zwraca: success or failed</p>
            </div>
            <div class="col-lg-3 border p-3">
                <p>url: /category/{id}</p>
                <p>Metoda: GET</p>
                <p>Żąda: ID kategorii z bazy danych</p>
                <p>Zwraca: array z produktami w danej kategorii</p>
            </div>
        </div>
    </div>
</div>
</body>
</html>
